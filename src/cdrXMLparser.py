#!/usr/bin/python
import xml.etree.cElementTree as ET
import sys
import urllib2

def xstr(s):
    if s is None:
        return ''
    return str(s)
FXML = sys.argv[1]
tree = ET.parse(FXML)
xcdr = tree.getroot()

if xcdr.find('variables').findtext('uuid'):
  callUuid = xcdr.find('variables').findtext('uuid')
else:
  callUuid = ''

if xcdr.find('variables').findtext('direction'):
  direction = xcdr.find('variables').findtext('direction')
else:
  direction = ''

if xcdr.find('variables').findtext('sip_to_user'):
  called = xcdr.find('variables').findtext('sip_to_user')
else:
  called = ''

if xcdr.find('variables').findtext('origination_caller_id_number'):
  caller = xcdr.find('variables').findtext('origination_caller_id_number')
else:
  caller = ''

if xcdr.find('variables').findtext('endpoint_disposition'):
  epDispo = xcdr.find('variables').findtext('endpoint_disposition')
else:
  epDispo = ''

if xcdr.find('variables').findtext('hangup_cause'):
  hangCause = xcdr.find('variables').findtext('hangup_cause')
else:
  hangCause = ''

if xcdr.find('variables').findtext('hangup_cause_q850'):
  isdCause = xcdr.find('variables').findtext('hangup_cause_q850')
else:
  isdCause = ''

if xcdr.find('variables').findtext('start_stamp'):
  startStamp = xcdr.find('variables').findtext('start_stamp')
else:
  startStamp = ''

if xcdr.find('variables').findtext('progress_media_stamp'):
  progressStamp = xcdr.find('variables').findtext('progress_media_stamp')
else:
  progressStamp = ''

if xcdr.find('variables').findtext('end_stamp'):
  stopStamp = xcdr.find('variables').findtext('end_stamp')
else:
  stopStamp = ''

if xcdr.find('variables').findtext('duration'):
  duration = xcdr.find('variables').findtext('duration')
else:
  duration = ''

if  xcdr.find('variables').findtext('billsec'):
  billsec = xcdr.find('variables').findtext('billsec')
else:
  billsec = ''

if xcdr.find('variables').findtext('skrtRecording'):
  recording = xcdr.find('variables').findtext('skrtRecording')
else:
  recording = ''

# decode url format
if epDispo:
  epDispo = urllib2.unquote(epDispo).decode('utf8')
if startStamp:
  startStamp = urllib2.unquote(startStamp).decode('utf8')
if progressStamp:
  progressStamp = urllib2.unquote(progressStamp).decode('utf8')
if stopStamp:
  stopStamp = urllib2.unquote(stopStamp).decode('utf8')

# print csv format
print ( callUuid+","+direction+","+caller+","+called+","+startStamp+","+progressStamp+","+stopStamp+","+duration+","+billsec+","+recording+","+epDispo+","+hangCause+","+isdCause+"")

